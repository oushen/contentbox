# ContentBox

ContentBox is small javascript library used to overlay different content on top 
of the current browser page. It's a snap to setup and works on all modern browsers.